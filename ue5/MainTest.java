import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * class MainTest manages the threads and gets the results.
 *
 */
public class MainTest {

	public static void main(String[] args) {
		DataCenter[] dataCenters = new DataCenter[4];
		for (int i = 1; i <= 4; i++) {
			int strength = (int) (Math.random() * 20) + 1;
			String name = "" + i;
			// Scanner s = new Scanner(System.in);
			// System.out.println("Set a name for dataCenter:");
			// String name = s.nextLine();
			// System.out.println("Set processing power for " + name);
			// int strength = s.nextInt();
			dataCenters[i - 1] = new DataCenter(strength, name);
			System.out.println("Datacenter " + name
					+ " created, process power:"
					+ dataCenters[i - 1].getProcessPower());
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i != j) {
					dataCenters[i].addOtherDataCenter(dataCenters[j]);
				}
			}
		}
		System.out.println("datacenters connected");
		ExecutorService es = Executors.newFixedThreadPool(5);
		for (DataCenter dataCenter : dataCenters) {
			es.execute(dataCenter);
		}
		System.out.println("starting jobs");
		JobGenerator jobgenerator = new JobGenerator(dataCenters);
		es.execute(jobgenerator);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		es.shutdownNow();
		System.out.println();
		System.out.println("Time's up, final scores:");
		System.out.println();
		for (int i = 0; i < 4; i++) {
			System.out.println(dataCenters[i].getTitle() + " made "
					+ dataCenters[i].getMoney() + " €, it stole "
					+ dataCenters[i].getStolenJobs() + " jobs.");
		}
		System.exit(0);
	}
}
