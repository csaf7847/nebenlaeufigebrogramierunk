
/**
 * class JobGenerator delivers various jobs to any {@link DataCenter} 
 *
 */
public class JobGenerator extends Thread {
	private DataCenter[] dataCenters;

	public JobGenerator(DataCenter[] dataCenters) {
		this.dataCenters = dataCenters;
	}

	public void run() {
		while (true) {
			try {
				int spawnRand = (int) (Math.random() * 3) + 1;
				Thread.sleep(spawnRand * 600);
				int dataCenterRand = (int) (Math.random() * 4);
				int moneyRand = (int) (Math.random() * 30) + 2;
				int workTimeRand = (int) (Math.random() * 60) + 1;
				Job job = new Job(workTimeRand, moneyRand);
				dataCenters[dataCenterRand].addToDeque(job);
			} catch (InterruptedException e) {
				break;
			}
		}
	}
}
