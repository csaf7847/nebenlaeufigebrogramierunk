import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * class DataCenter receives jobs either by the {@link JobGenerator} or by
 * stealing jobs from other DataCenters. After receiving the job, it will be
 * worked on until the processPower outweighs the workTime.
 * 
 */

public class DataCenter extends Thread {

	private int processPower = 0;
	private int money = 0;
	private LinkedBlockingDeque<Job> deque;
	private Job currentJob = null;
	private int stolenJobs = 0;
	private ArrayList<DataCenter> otherDataCenters;
	private String name = null;

	public DataCenter(int processPower, String name) {
		this.processPower = processPower;
		this.deque = new LinkedBlockingDeque<Job>();
		this.otherDataCenters = new ArrayList<DataCenter>();
		this.name = name;
	}

	public void run() {
		while (true) {
			// try and get a job...however.
			try {
				// take a job...
				if ((this.currentJob = deque.pollFirst()) != null) {
					System.out.println("Datacenter " + this.name
							+ " took a job!");
				} else { // ...or steal one.
					stealJob();
				}
				if (this.currentJob != null) {
					// do the job, count the
					// money.
					System.out.println("dataCenter " + this.name
							+ " working on job with "
							+ currentJob.getWorkTime());
					int countDown = this.currentJob.getWorkTime();
					while (countDown > 0) {
						Thread.sleep(1000);
						countDown -= this.processPower;
					}
					this.money += currentJob.getMoney();
					System.out.println("Datacenter " + this.name
							+ ": Job done, earned " + currentJob.getMoney()
							+ ", having now " + this.money);
					// release done Job to get a new one.
					this.currentJob = null;
				}
			} catch (InterruptedException e) {
				break;
			}
		}
	}

	public Job giveAwayJob() {
		Job job = null;
		// this job can only be stolen if the
		// dataCenter is busy on
		// another one.
		if (this.currentJob != null) {
			job = deque.pollLast();
			if (job != null) {
				System.out.println("They took my jooob!, " + this.name);
			}
		}
		return job;
	}

	public void stealJob() {
		Job bestJob = null;
		DataCenter otherDataCenter = null;
		// check the other DataCenters for free jobs.
		for (int i = 0; i < this.otherDataCenters.size(); i++) {
			// reserve jobs (if available) from all dataCenters
			Job stolenJob = this.otherDataCenters.get(i).giveAwayJob();
			if (stolenJob == null) {
				continue;
			} else if (bestJob == null) {
				bestJob = stolenJob;
				otherDataCenter = this.otherDataCenters.get(i);
				// if the reserved job is better than the one until now, give
				// the old best job back to its dataCenter.
			} else if (stolenJob.getWorth() > bestJob.getWorth()) {
				otherDataCenter.addToDeque(bestJob);
				bestJob = stolenJob;
				otherDataCenter = this.otherDataCenters.get(i);
				// if reserved job is not as attractive as the one before, just
				// give it back to the deque of the same dataCenter.
			} else if (stolenJob.getWorth() < bestJob.getWorth()) {
				otherDataCenters.get(i).addToDeque(stolenJob);
			}
		}
		if (bestJob != null) {
			stolenJobs++;
			this.currentJob = bestJob;
			System.out.println("Job STOLEN by " + this.name);
		}
	}

	public int getProcessPower() {
		return processPower;
	}

	public int getMoney() {
		return money;
	}

	public int getStolenJobs() {
		return this.stolenJobs;
	}

	public void addOtherDataCenter(DataCenter dataCenter) {
		this.otherDataCenters.add(dataCenter);
	}

	public synchronized void addToDeque(Job job) {
		System.out.println("try adding job with " + job.getWorkTime()
				+ " effort and " + job.getWorth() + " worth to queue of "
				+ this.name);
		this.deque.offerLast(job);
	}

	public String getTitle() {
		return name;
	}
}