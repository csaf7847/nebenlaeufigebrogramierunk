/**
 * class Job contains the relevant information for dataCenters to work with.
 *
 */
public class Job {

	private int workTime = 0;
	private int money = 0;

	public Job(int workTime, int money) {
		this.workTime = workTime;
		this.money = money;
	}

	public int getWorkTime() {
		return workTime;
	}

	public int getMoney() {
		return money;
	}

	public int getWorth() {
		return money / workTime;
	}
}
