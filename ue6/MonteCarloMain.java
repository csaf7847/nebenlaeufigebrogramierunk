// Monte Carlo simulation of a European call option

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//-------------------------------
class MonteCarloMain {
	final static double t = 1.0, r = 0.05;

	// -------------------------------

	public static void main(String[] args) throws InterruptedException,
			ExecutionException {
		System.out
				.println("Please enter amount of Callables (or just hit enter for 1000 Callables)");
		int numberOfRunnables = -1;
		Scanner s2 = new Scanner(System.in);
		try {
			numberOfRunnables = Integer.parseInt(s2.nextLine());
		} catch (NumberFormatException nfe) {
			numberOfRunnables = 1000;
		}
		// singleThread, very slow
		System.out.println("SingleThreaded Executor starts");
		ThreadPoolExecutor tpe = new ThreadPoolExecutor(1, 1, 15,
				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(
						numberOfRunnables));
		doWork(numberOfRunnables, tpe);
		System.out.println("Executor done");
		// fast one, same as a fixedSizeThreadPool
		System.out.println("Fast multicore Executor starts");
		ThreadPoolExecutor tpe1 = new ThreadPoolExecutor(8, 8, 15,
				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(
						numberOfRunnables));
		tpe1.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		ExecutorService es1 = Executors.unconfigurableExecutorService(tpe1);
		doWork(numberOfRunnables, es1);
		System.out.println("Executor 1 done");
		// medium, this would be nearly a cachedSizeThreadPool
		System.out.println("Flexible single/multicore Executor starts");
		ThreadPoolExecutor tpe3 = new ThreadPoolExecutor(1, 8, 15,
				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(
						numberOfRunnables));
		doWork(numberOfRunnables, tpe3);
		System.out.println("Executor 2 done");
		// very little threads
		System.out.println("Weak CPU Executor starts");
		ExecutorService tpe4 = new ThreadPoolExecutor(0, 2, 15,
				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(
						numberOfRunnables));
		doWork(numberOfRunnables, tpe4);
		System.out.println("Executor 3 done");
		// too small queue
		System.out.println("Minimal queue capacity Executor starts");
		ThreadPoolExecutor tpe5 = new ThreadPoolExecutor(8, 8, 15,
				TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(
						numberOfRunnables / 8));
		tpe5.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		ExecutorService es4 = Executors.unconfigurableExecutorService(tpe5);
		doWork(numberOfRunnables, es4);
		System.out.println("Executor 4 done");
		// es = Executors.unconfigurableExecutorService(Executors
		// .newFixedThreadPool(8));
		// bad example, not as much threads as available:
		// es = Executors.unconfigurableExecutorService(Executors
		// .newFixedThreadPool(4));
	}

	private static void doWork(int numberOfRunnables, ExecutorService es)
			throws InterruptedException, ExecutionException {
		ArrayList<FutureTask<Double>> runnerList = new ArrayList<FutureTask<Double>>(
				numberOfRunnables);
		long startTime = System.currentTimeMillis();
		final double exprT = Math.exp(-r * t);
		int allIterations = numberOfRunnables * 1000;
		double sum = 0.0;
		double value;
		for (int j = 1; j <= allIterations; j += 1000) {
			try {
				FutureTask<Double> monteCarloRunner = new FutureTask<Double>(
						new MonteCallable(j));
				es.submit(monteCarloRunner);
				runnerList.add(monteCarloRunner);
				// sum += new MonteCallable2(j).call();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		es.shutdown();
		while (!es.isTerminated()) {
		}
		for (int i = 0; i < runnerList.size(); i++) {
			sum += runnerList.get(i).get();
		}
		value = sum * exprT / (double) allIterations;
		System.out.println("value=" + value);
		long stopTime = System.currentTimeMillis();
		long resultTime = stopTime - startTime;

		System.out.println("Executor result: " + value);
		System.out.println("Time needed for run: " + ((double) resultTime)
				/ 1000.0 + " seconds");
		es.shutdownNow();
	}
}

class MonteCallable implements Callable<Double> {
	final static double t = 1.0, r = 0.05;
	private int startValue;

	public MonteCallable(int startValue) {
		this.startValue = startValue;
	}

	@Override
	public Double call() throws Exception {
		final double sig = 0.2;
		final double dt = t / 100.0;
		final double nuDt = (r - 0.5 * sig * sig) * dt;
		final double sigSqrtDt = sig * Math.sqrt(dt);
		Random random = new Random();
		final double s0 = 100.0, k = 100.0;
		double eps;
		double st;
		double ct = 0.0;
		for (int j2 = startValue; j2 <= startValue + 1000; j2++) {
			st = s0;
			for (int i = 1; i <= 100; i++) {
				eps = random.nextGaussian();
				st *= Math.exp(nuDt + sigSqrtDt * eps);
			}
			ct += Math.max(0.0, st - k);
		}
		// System.out.println(startValue);
		return ct;
	}

}